# Sitemap

Provides integration with the geoPHP library: [GeoPHP](https://geoPHP.net)

This module does not provide any direct functionality to end-users or site-administrators. Install it only if
another module requires it.

GeoPHP is a open-source native PHP library for doing geometry operations. It is written entirely in PHP and can
therefore run on shared hosts. It can read and write a wide variety of formats (WKT, WKB, GeoJSON, KML, GPX,GeoRSS).It works with all Simple-Feature geometries (Point, LineString, Polygon, GeometryCollection etc.) and can be used to
get centroids, bounding-boxes, area, and a wide variety of other useful information.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/geophp).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/geophp).


## Table of contents

- Requirements
- Installation
- Sitemap term path (and Pathauto)
- Configuration
- Maintainers


## Requirements

This module does not have any dependency on any other module.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

This module does not have any configuration.


## Maintainers

- Brandon Morrison - [Brandonian](https://www.drupal.org/u/brandonian)
- Neslee Canil Pinto - [Neslee Canil Pinto](https://www.drupal.org/u/neslee-canil-pinto)
- Patrick Hayes - [phayes](https://www.drupal.org/u/phayes)
- Pablo López - [plopesc](https://www.drupal.org/u/plopesc)